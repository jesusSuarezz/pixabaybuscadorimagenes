import React, { useState, useEffect } from 'react';

import Form from './components/Form';
import ListadoImagenes from './components/ListadoImagenes';

function App() {
	// guardar la palabra que esta en el input
	// Guardar el resultado de la api
	const [busqueda, guardarBusqueda] = useState('');

	//para que no se ejecute a cada rato la peticion en useEffect
	const [imagenes, guardarImagenes] = useState([]);
	//State para la paginacion
	const [paginaactual, guardarPaginaActual] = useState(1);
	const [totalpaginas, guardarTotalPaginas] = useState(1);

	useEffect(() => {
		//Si el input busqueda esta vacio se detiene aqui para no hacer la consulta
		if (busqueda === '') return;

		//Creamos una funcion para hacer la peticion con asyncAwait
		const consultarAPI = async () => {
			const imagenesPorPagina = 30;
			const apiKEY = '18969967-5c16b4490f250da226c6d3586';
			const urlAPI = `https://pixabay.com/api/?key=${apiKEY}&q=${busqueda}&per_page=${imagenesPorPagina}&page=${paginaactual}`;
			//consutlamos la api con fetch y guardamos la respuesta en response
			let response = await fetch(urlAPI);
			//convertimos la respuesta del servidor a formato json
			response = await response.json();
			//let response = await (await fetch(urlAPI)).json(); //esta es lo mismo que lo de arriba en una sola linea
			console.log(response.total);

			//guardamos el resultado de la peticion
			guardarImagenes(response.hits);

			//calcular el total de paginas que va haber para mostrar las imagenes
			const calculaTotalPaginas = Math.ceil(
				response.totalHits / imagenesPorPagina
			);
			guardarTotalPaginas(calculaTotalPaginas);
		};

		//Lamamos a la fucntion de la api
		consultarAPI();

    //Mover la pantalla hacia arriba cuando de click en el boton siguiente o anterior
    //se mueve con una funcion que apuntan a la clase jumbotron
    const jumbotron = document.querySelector('.jumbotron');
    jumbotron.scrollIntoView({behavior:'smooth'})
	}, [busqueda, paginaactual]);

	//Definir la paginaAnterior
	const paginaAnterior = () => {
		const nuevaPaginaActual = paginaactual - 1;
		if (nuevaPaginaActual === 0) return;
		guardarPaginaActual(nuevaPaginaActual);
	};

	const paginaSiguiente = () => {
		const nuevaPaginaActual = paginaactual + 1;
		if (nuevaPaginaActual > totalpaginas) return;
		guardarPaginaActual(nuevaPaginaActual);
	};

	return (
		<div className="container">
			<div className="jumbotron">
				<p className="lead text-center">Buscador de imagenes (Jesus Suarez)</p>

				<Form guardarBusqueda={guardarBusqueda} />
			</div>
			<div className="row justify-content-center">
				<ListadoImagenes imagenes={imagenes} />

			{paginaactual === 1 ? null : (
				<button
					type="button"
					className="btn btn-info mr-1"
					onClick={paginaAnterior}
          >
					&laquo; Anterior
				</button>
			)}

			{paginaactual === totalpaginas ? null : (
        <button
        type="button"
					className="btn btn-info mr-1"
					onClick={paginaSiguiente}
          >
					Siguiente &raquo;
				</button>
			)}
      </div>
		</div>
	);
}

export default App;
