import React from 'react';

const Error = ({ mensaje }) =>
	mensaje ? (
		<p className="my-3 p-4 text-center alert-primary alert">
			{mensaje}
		</p>
	) : null;

export default Error;
