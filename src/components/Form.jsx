import React, { useState } from 'react';
import Error from './Error';

const Form = ({guardarBusqueda}) => {
	// Como el form es solo un input no necesita una funcion mas que el onchange
	const [tema, guardarTema] = useState('');
    console.log(tema);
    
	const [error, guardarError] = useState(false);

	const buscarImagenes = (e) => {
		e.preventDefault();

		//Validar form
		if (tema.trim() === '') {
			guardarError(true);
			return;
        }
        
        guardarError(false);
        
		//enviar el tema de busqueda hacia el componente pricipal
        guardarBusqueda(tema)
	};

	return (
		<form onSubmit={buscarImagenes}>
			<div className="row">
				<div className="form-group col-md-8">
					<input
						type="text"
						className="form-control form-control-lg"
						placeholder="Busca la imagen"
						// Funcion que escucha cambios del value del input
						onChange={(e) => guardarTema(e.target.value)}
					/>
				</div>
				<div className="form-group col-md-4">
					<input
						type="submit"
						className="btn btn-lg btn-danger btn-block"
						value="Buscar"
					/>
				</div>
			</div>
			{error ? <Error mensaje="Escribe el tema para buscar!!" /> : null}
		</form>
	);
};

export default Form;
